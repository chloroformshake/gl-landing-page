let players = document.getElementsByClassName('youtube-player')
const playButton = document.getElementById('play-btn')

let loadPlayer = function (event) {

    let target = players[0]

    let iframe = document.createElement('iframe')
    iframe.src = 'https://www.youtube.com/embed/' + target.dataset.videoId + '?autoplay=1'
    iframe.setAttribute('frameborder', 0)

    target.replaceChild(iframe, target.firstElementChild)

    event.target.style.display = "none"
}

playButton.addEventListener('click', loadPlayer)