let accordion = document.getElementsByClassName("accordion")
let i

const closeAccordion = () => {
    for (i = 0; i < accordion.length; i++) {

        if (accordion[i].classList.length > 1) { accordion[i].classList.remove("accordion-active") }

        let eachPanel = accordion[i].nextElementSibling
        eachPanel.style.height = 0
    }
}

for (i = 0; i < accordion.length; i++) {

    accordion[i].addEventListener("click", function () {

        let panel = this.nextElementSibling

        if (this.classList.contains("accordion-active")) {
            this.classList.toggle("accordion-active")
            panel.style.height = 0
        } else {
            closeAccordion()
            this.classList.toggle("accordion-active")
            panel.style.height = panel.scrollHeight + "px"
        }
    });
}
