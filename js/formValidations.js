const nameEl = document.querySelector('#name');
const emailEl = document.querySelector('#email');
const mobileEl = document.querySelector('#mobile');
const experienceEl = document.querySelector('#experience');
const selectEl = document.querySelector('#select-field')
const checkboxEl = document.querySelector('#checkbox')
const submitEl = document.querySelector('#submit');
const formEl = document.querySelector('#header-form');

(() => {
    console.log(localStorage);
    if ('name' in localStorage) { nameEl.value = localStorage.getItem('name') }
    if ('email' in localStorage) { emailEl.value = localStorage.getItem('email') }
    if ('mobile' in localStorage) { mobileEl.value = localStorage.getItem('mobile') }
    if ('experience' in localStorage) { experienceEl.value = localStorage.getItem('experience') }
    if ('select' in localStorage) { selectEl.value = localStorage.getItem('select') }
})();


const checkName = () => {

    let valid = false;
    const name = nameEl.value.trim();

    if (!isRequired(name)) {
        showError(nameEl, 'Name cannot be blank.');
    } else {
        showSuccess(nameEl);
        valid = true;
    }
    return valid;
};

const checkEmail = () => {

    let valid = false;
    const email = emailEl.value.trim();

    if (!isRequired(email)) {
        showError(emailEl, 'Email cannot be blank.');
    } else if (!isEmailValid(email)) {
        showError(emailEl, 'Email is not valid.')
    } else {
        showSuccess(emailEl);
        valid = true;
    }
    return valid;
};

const checkMobile = () => {

    let valid = false;
    const mobile = mobileEl.value.trim();

    if (!isRequired(mobile)) {
        showError(mobileEl, 'Mobile No. cannot be blank.');
    } else if (!isNumberValid(mobile)) {
        showError(mobileEl, 'Input a valid Mobile No.');
    } else {
        showSuccess(mobileEl);
        valid = true;
    }
    return valid;
};

const checkExperience = () => {

    let valid = false
    const experience = experienceEl.value.trim()

    if (!isRequired(experience)) {
        showError(experienceEl, 'Work Experience cannot be blank.')
    } else if (parseInt(experience) >= 0 && parseInt(experience) < 99) {
        showSuccess(experienceEl)
        valid = true
    } else {
        showError(experienceEl, 'Input a number in 0-99 range')
    }
    return valid
}

const checkSelect = () => {

    let valid = false
    const organization = selectEl.value.trim()

    if (organization === 'Current Organization') {
        showError(selectEl, 'Please select an option.')
    } else {
        showSuccess(selectEl)
        valid = true
    }
    return valid
}

const isEmailValid = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

const isNumberValid = (mobile) => {
    const re = new RegExp("[6-9]{1}[0-9]{9}");
    return re.test(mobile);
};

const isRequired = value => value === '' ? false : true;

const showError = (input, message) => {

    const formField = input.parentElement;

    formField.classList.remove('success');
    formField.classList.add('error');

    const error = formField.querySelector('small');
    error.textContent = message;
};

const showSuccess = (input) => {

    const formField = input.parentElement;

    formField.classList.remove('error');
    formField.classList.add('success');

    const error = formField.querySelector('small');
    error.textContent = '';
}

const showApplied = (c) => {

    const submitField = submitEl.parentElement
    const appliedText = submitField.querySelector('small')

    if (c === 5) { appliedText.textContent = 'Successfully Applied!' }
    else { appliedText.textContent = ' ' }
}

formEl.addEventListener('submit', function (e) {

    e.preventDefault();

    let c = 0
    if (checkName()) { localStorage.setItem('name', nameEl.value); c += 1; }
    if (checkEmail()) { localStorage.setItem('email', emailEl.value); c += 1; }
    if (checkMobile()) { localStorage.setItem('mobile', mobileEl.value); c += 1; }
    if (checkExperience()) { localStorage.setItem('experience', experienceEl.value); c += 1; }
    if (checkSelect()) { localStorage.setItem('select', selectEl.value); c += 1; }

    showApplied(c)
});

formEl.addEventListener('input', function (e) {

    switch (e.target.id) {
        case 'name':
            checkName();
            break;
        case 'email':
            checkEmail();
            break;
        case 'mobile':
            checkMobile();
            break;
        case 'experience':
            checkExperience();
            break;
    }
})

selectEl.addEventListener('change', checkSelect)

const enableSubmit = function (e) {
    submitEl.disabled = !this.checked
};

checkboxEl.addEventListener('change', enableSubmit);