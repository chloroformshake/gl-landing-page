const scrollToTopButton = document.getElementById('top-link-btn')
const form = document.getElementById('header-form')

const scrollFunc = (entries) => {

  if (!entries[0].isIntersecting) {
    scrollToTopButton.parentElement.className = "top-link show"
  } else {
    scrollToTopButton.parentElement.className = "top-link hide"
  }
}

const observer = new IntersectionObserver(scrollFunc, { threshold: [0.2] })

observer.observe(form)

const scrollToTop = () => {

  const c = document.documentElement.scrollTop

  if (c > 0) {
    window.requestAnimationFrame(scrollToTop)
    window.scrollTo(0, c - c / 15)
  }
}

scrollToTopButton.onclick = function (e) {
  e.preventDefault()
  scrollToTop()
}